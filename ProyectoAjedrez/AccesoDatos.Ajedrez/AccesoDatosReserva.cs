﻿using Entidades.Ajedrez;
using System.Data;
using System.Windows.Forms;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosReserva
    {
        ConexionBDAjedrez c = new ConexionBDAjedrez();
        public string Insertar(Reserva reserva)
        {
            return c.Comando(string.Format("insert into reserva values ({0},'{1}','{2}',{3},{4})",
                reserva.IdReserva, reserva.FechaEntrada, reserva.FechaSalida, reserva.FkIdHotel, reserva.FkIdParticipante));
        }
        public string Actualizar(Reserva reserva)
        {
            return c.Comando(string.Format("update reserva set fechaentrada = '{0}', fechasalida = '{1}', fkidhotel = {2}, fkidparticipante = {3} " +
                "where idreserva = {4}", reserva.FechaEntrada, reserva.FechaSalida, reserva.FkIdHotel, reserva.FkIdParticipante, reserva.IdReserva));
        }
        public string Eliminar(Reserva reserva)
        {
            return c.Comando(string.Format("Delete from reserva where idreserva = {0}", reserva.IdReserva));
        }
        public DataSet Listado(string q, string tabla)
        {
            return c.Mostrar(q, tabla);
        }
        public int TraerIdHotel(string nombre)
        {
            DataTable dt = new DataTable();
            dt = c.Mostrar(string.Format("Select idhotel from hotel where nombre = '{0}'", nombre), "hotel").Tables[0];
            DataRow r = dt.Rows[0];
            return int.Parse(r["idhotel"].ToString());
        }
        public string GetHotel(int idhotel)
        {
            DataTable dt = new DataTable();
            dt = c.Mostrar(string.Format("Select nombre from hotel where idhotel = {0}", idhotel), "hotel").Tables[0];
            DataRow r = dt.Rows[0];
            return r["nombre"].ToString();
        }

        public int TraerIdParticipante(string nombre)
        {
            DataTable dt = new DataTable();
            dt = c.Mostrar(string.Format("Select nsocio from participante where nombre = '{0}'", nombre), "participante").Tables[0];
            DataRow r = dt.Rows[0];
            return int.Parse(r["nsocio"].ToString());
        }
        public string GetParticipante(int nsocio)
        {
            DataTable dt = new DataTable();
            dt = c.Mostrar(string.Format("select nombre from participante where nsocio = {0}", nsocio), "participante").Tables[0];
            DataRow r = dt.Rows[0];
            return r["nombre"].ToString();
        }
        public void llenarCombo(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = Listado(script, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
        //Borra esto
        public void llenarCombo2(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = Listado(script, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
    }
}
