﻿using System.Data;
using System.Windows.Forms;
using Entidades.Ajedrez;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosCampeonato
    {
        ConexionBDAjedrez ca = new ConexionBDAjedrez();
        public string Insertar(Campeonato camp)
        {
            return ca.Comando(string.Format("insert into Campeonato values({0},'{1}','{2}',{3})",
                camp.IdCampeonato, camp.Nombre, camp.Tipo, camp.IdParticipante));
        }

        public string Borrar(Campeonato camp)
        {
            return ca.Comando(string.Format("delete from Campeonato where idcampeonato = {0}", camp.IdCampeonato));
        }

        public string Actualizar(Campeonato camp)
        {
            return ca.Comando(string.Format("update Campeonato set fkidparticipante = {0}, nombre = '{1}', tipo = '{2}' where idcampeonato = {3}", camp.IdParticipante, camp.Nombre, camp.Tipo, camp.IdCampeonato));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public string TraerParticipante(int nombre)
        {
            DataTable dt = new DataTable();
            dt = ca.Mostrar(string.Format("select nombre from Participante where nsocio  = {0}", nombre), "Participante").Tables[0];
            DataRow dr = dt.Rows[0];
            return dr["nombre"].ToString();
        }
        public int TraerIdParticipante(string nsocio)
        {
            DataTable dt = new DataTable();
            dt = ca.Mostrar(string.Format("select nsocio from Participante where nombre  = '{0}'", nsocio), "Participante").Tables[0];
            DataRow dr = dt.Rows[0];
            return int.Parse(dr["nsocio"].ToString());
        }
        public void llenarComboPartida(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = Listado(script, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
    }
}