﻿using Entidades.Ajedrez;
using System.Data;
using System.Windows.Forms;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosSala
    {
        ConexionBDAjedrez c = new ConexionBDAjedrez();
        public string Insertar(Sala sala)
        {
            return c.Comando(string.Format("insert into sala values({0},{1},{2},'{3}')",
                sala.IdSala, sala.FkIdHotel, sala.Capacidad, sala.Medios));
        }

        public string Eliminar(Sala sala)
        {
            return c.Comando(string.Format("Delete from sala where idsala = {0}", sala.IdSala));
        }

        public string Actualizar(Sala sala)
        {
            return c.Comando(string.Format("Update sala set fkidhotel = {0}, capacidad = {1}, medios = '{2}' where idsala = {3}", sala.FkIdHotel, sala.Capacidad, sala.Medios, sala.IdSala));
        }

        public System.Data.DataSet Listado(string q, string tabla)
        {
            return c.Mostrar(q, tabla);
        }
        public int TraerIdHotel(string nombre)
        {
            DataTable dt = new DataTable();
            dt = c.Mostrar(string.Format("Select idhotel from hotel where nombre = '{0}'", nombre), "hotel").Tables[0];
            DataRow r = dt.Rows[0];
            return int.Parse(r["idhotel"].ToString());
        }
        public string GetHotel(int idhotel)
        {
            DataTable dt = new DataTable();
            dt = c.Mostrar(string.Format("select nombre from hotel where idhotel = {0}", idhotel), "hotel").Tables[0];
            DataRow r = dt.Rows[0];
            return r["nombre"].ToString();
        }
        public void llenarCombo(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = Listado(script, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
    }
}
