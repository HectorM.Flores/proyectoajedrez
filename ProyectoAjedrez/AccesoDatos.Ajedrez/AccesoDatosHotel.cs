﻿using System.Data;
using Entidades.Ajedrez;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosHotel
    {
        ConexionBDAjedrez con = new ConexionBDAjedrez();
        public string Insertar(Hotel hotel)
        {
            return con.Comando(string.Format("insert into hotel values({0},'{1}','{2}','{3}')",
                hotel.IdHotel, hotel.Nombre, hotel.Direccion, hotel.Telefono));
        }

        public string Actualizar(Hotel hotel)
        {
            return con.Comando(string.Format("update hotel set nombre = '{0}' where idhotel = {1}", hotel.Nombre, hotel.IdHotel));
        }

        public string Eliminar(Hotel hotel)
        {
            return con.Comando(string.Format("Delete from hotel where idhotel = {0}", hotel.IdHotel));
        }

        public DataSet Listado(string q, string tabla)
        {
            return con.Mostrar(q, tabla);
        }
    }
}
