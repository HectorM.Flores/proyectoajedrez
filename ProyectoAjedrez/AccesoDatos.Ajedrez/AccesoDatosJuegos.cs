﻿using System.Data;
using System.Windows.Forms;
using Entidades.Ajedrez;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosJuegos
    {
        ConexionBDAjedrez _conexion = new ConexionBDAjedrez();
        public string GuardarJuego(Juegos juego)
        {
            return _conexion.Comando(string.Format("insert into juego values ({0},{1},'{2}')", juego.IdJugador, juego.Idpartida, juego.Color));
        }
        public string ActualizarJuego(Juegos juego)
        {
            return _conexion.Comando(string.Format("update juego set color = '{0}', idjugador = {1} where idpartida ={2}", juego.Color, juego.IdJugador, juego.Idpartida));
        }
        public string EliminarJuego(Juegos juego)
        {
            return _conexion.Comando(string.Format("delete from juego where idpartida={0}", juego.Idpartida));
        }
        public DataSet ObtenerJuegos(string consulta, string tabla)
        {
            return _conexion.Mostrar(consulta, tabla);
        }
        public void LLenarPartida(ComboBox combo, string consulta, string tabla)
        {
            combo.DataSource = _conexion.Mostrar(consulta, tabla).Tables[0];
            combo.DisplayMember = "idpartida";
        }
        public void LlenarJugador(ComboBox combo, string consulta, string tabla)
        {
            combo.DataSource = _conexion.Mostrar(consulta, tabla).Tables[0];
            combo.DisplayMember = "idjugador";
        }
    }
}
