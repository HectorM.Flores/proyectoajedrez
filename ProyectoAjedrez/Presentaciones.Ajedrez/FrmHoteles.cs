﻿using System;
using System.Windows.Forms;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
    public partial class FrmHoteles : Form
    {
        LogicaHotel lh = new LogicaHotel();
        int fila = 0;
        public FrmHoteles()
        {
            InitializeComponent();
        }
        void Actualizar()
        {
            DtgHotel.DataSource = lh.Mostrar(string.Format("Select idhotel as IdHotel, nombre as Nombre, direccion as Direccion, telefono as Telefono from hotel where nombre like '%{0}%'",
                txtBuscar.Text), "Hotel").Tables[0];
            DtgHotel.AutoResizeColumns();
        }
        void Limpiar()
        {
            txtIdHotel.Clear();
            txtNombre.Clear();
            txtDireccion.Clear();
            txtTelefono.Clear();
        }

        private void FrmHoteles_Load(object sender, EventArgs e)
        {
            Actualizar();
            BtnActualizar.Enabled = false;
            BtnEliminar.Enabled = false;
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            lh.Insertar(new Hotel(int.Parse(txtIdHotel.Text), txtNombre.Text, txtDireccion.Text, txtTelefono.Text));
            Actualizar();
            Limpiar();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                lh.Actualizar(new Hotel(int.Parse(txtIdHotel.Text), txtNombre.Text, txtDireccion.Text, txtTelefono.Text));
                Actualizar();
                BtnGuardar.Enabled = true;
                txtIdHotel.Enabled = false;
                txtDireccion.Enabled = true;
                txtTelefono.Enabled = true;
                BtnEliminar.Enabled = false;
                BtnActualizar.Enabled = false;
                Limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show("Debes seleccionar el dato a actualizar");
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Estas seguro de eliminar el hotel " + txtNombre.Text + " ?", "¡¡Importante!!", MessageBoxButtons.YesNoCancel);
                if (rs == DialogResult.Yes)
                {
                    lh.Eliminar(new Hotel(int.Parse(txtIdHotel.Text),"","",""));
                    Limpiar();
                    Actualizar();
                    BtnEliminar.Enabled = false;
                    BtnActualizar.Enabled = false;
                    BtnGuardar.Enabled = true;
                    txtIdHotel.Enabled = true;
                    txtDireccion.Enabled = true;
                    txtTelefono.Enabled = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Debes selecionar el dato a eliminar");
            }
        }

        private void DtgHotel_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BtnGuardar.Enabled = false;
            txtNombre.Focus();
            txtIdHotel.Enabled = false;
            BtnEliminar.Enabled = true;
            BtnActualizar.Enabled = true;
            txtDireccion.Enabled = false;
            txtTelefono.Enabled = false;

            fila = e.RowIndex;
            txtIdHotel.Text = DtgHotel.Rows[fila].Cells[0].Value.ToString();
            txtNombre.Text = DtgHotel.Rows[fila].Cells[1].Value.ToString();
            txtDireccion.Text = DtgHotel.Rows[fila].Cells[2].Value.ToString();
            txtTelefono.Text = DtgHotel.Rows[fila].Cells[3].Value.ToString();

        }
    }
}
