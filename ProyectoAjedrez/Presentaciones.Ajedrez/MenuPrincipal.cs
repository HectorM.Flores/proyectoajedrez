﻿using System;
using System.Windows.Forms;

namespace Presentaciones.Ajedrez
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void OptSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OptHotel_Click(object sender, EventArgs e)
        {
            FrmHoteles fh = new FrmHoteles();
            fh.ShowDialog();
        }

        private void OptSala_Click(object sender, EventArgs e)
        {
            FrmSalas fs = new FrmSalas();
            fs.ShowDialog();
        }

        private void OptReserva_Click(object sender, EventArgs e)
        {
            FrmReservas fr = new FrmReservas();
            fr.ShowDialog();
        }

        private void OptParticipante_Click(object sender, EventArgs e)
        {
            FrmParticipantes fr = new FrmParticipantes();
            fr.ShowDialog();
        }

        private void OptJugador_Click(object sender, EventArgs e)
        {
            FrmJugadores fr = new FrmJugadores();
            fr.ShowDialog();
        }

        private void OptArbitro_Click(object sender, EventArgs e)
        {
            FrmArbitros fr = new FrmArbitros();
            fr.ShowDialog();
        }

        private void OptMovimientos_Click(object sender, EventArgs e)
        {
            FrmMovimiento fm = new FrmMovimiento();
            fm.ShowDialog();
        }

        private void OptCampeonato_Click(object sender, EventArgs e)
        {
            FrmCampeonato fc = new FrmCampeonato();
            fc.ShowDialog();
        }

        private void OptPais_Click(object sender, EventArgs e)
        {
            FrmPais fp = new FrmPais();
            fp.ShowDialog();
        }

        private void partidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPartidas fp = new FrmPartidas();
            fp.ShowDialog();
        }

        private void OptJuego_Click(object sender, EventArgs e)
        {
            FrmJuegos fj = new FrmJuegos();
            fj.ShowDialog();
        }
    }
}
