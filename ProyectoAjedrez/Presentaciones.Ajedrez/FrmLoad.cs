﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentaciones.Ajedrez
{
    public partial class FrmLoad : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
            (
            int nLeftRect,
            int nTopRect,
            int RightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse

            );
        public FrmLoad()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));
            prograssbar.Value = 0;
        }

        private void loading_Tick(object sender, EventArgs e)
        {
            prograssbar.Value += 1;
            prograssbar.Text = prograssbar.Value.ToString() + "%";

            if (prograssbar.Value == 100)
            {
                this.Hide();
                loading.Enabled = false;
                MenuPrincipal fc = new MenuPrincipal();
                fc.ShowDialog();
            }
        }
    }
}
