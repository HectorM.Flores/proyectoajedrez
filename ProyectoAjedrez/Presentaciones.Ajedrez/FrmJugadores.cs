﻿using System;
using System.Windows.Forms;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;


namespace Presentaciones.Ajedrez
{
    public partial class FrmJugadores : Form
    {
        LogicaJugador jl = new LogicaJugador();
        int fila = 0;
        int posicion;
        public FrmJugadores()
        {
            InitializeComponent();
        }

        void Actualizar()
        {
            DtgJugadores.DataSource = jl.Listado(string.Format(
                "select idjugador as IDJugador, nivel as Nivel from Jugador where idjugador like '%{0}%'",
                TxtBuscar.Text), "Jugador").Tables[0];
            DtgJugadores.AutoResizeColumns();
        }

        void Limpiar()
        {
            CmbIdJugador.Text = "";
            TxtNombre.Clear();
            TxtNivel.Clear();
        }

        private void FrmJugadores_Load(object sender, EventArgs e)
        {
            Actualizar();
            jl.LlenarParticipantes(CmbIdJugador, "select nsocio from Participante", "Participante");
            BtnEliminar.Enabled = false;
            BtnModificar.Enabled = false;
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void DtgJugadores_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            CmbIdJugador.Text = DtgJugadores.Rows[fila].Cells[0].Value.ToString();
            TxtNivel.Text = DtgJugadores.Rows[fila].Cells[1].Value.ToString();
            TxtNombre.Text = jl.GetParticipante(int.Parse(CmbIdJugador.Text));
            BtnGuardar.Enabled = false;
            BtnEliminar.Enabled = true;
            BtnModificar.Enabled = true;
            CmbIdJugador.Enabled = false;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            jl.Insertar(new Jugador(int.Parse(CmbIdJugador.Text), int.Parse(TxtNivel.Text)));
            Actualizar();
            Limpiar();
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                jl.Actualizar(new Jugador(int.Parse(CmbIdJugador.Text), int.Parse(TxtNivel.Text)));
                Actualizar();
                Limpiar();
                BtnGuardar.Enabled = true;
                BtnEliminar.Enabled = false;
                BtnModificar.Enabled = false;
                CmbIdJugador.Enabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de actualizar, necesitas hacer click en el dato que deseas modificar.");
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Esta seguro de eliminar al jugador " + TxtNombre.Text + "?",
                "Atencion!!!", MessageBoxButtons.YesNoCancel);
                if (rs == DialogResult.Yes)
                {
                    jl.Borrar(new Jugador(int.Parse(CmbIdJugador.Text), 0));
                    Limpiar();
                    Actualizar();
                    BtnGuardar.Enabled = true;
                    BtnEliminar.Enabled = false;
                    BtnModificar.Enabled = false;
                    CmbIdJugador.Enabled = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de eliminar necesitas hacer click en el dato que deseas eliminar.");
            }
        }

        void Asignar()
        {
            string x = CmbIdJugador.Text;
            if (!x.Equals("System.Data.DataRowView"))
            {
                posicion = int.Parse(CmbIdJugador.Text);
            }
        }

        private void CmbIdJugador_SelectedIndexChanged(object sender, EventArgs e)
        {
            Asignar();
            if (posicion > 0)
            {
                TxtNombre.Text = jl.GetParticipante(int.Parse(CmbIdJugador.Text));
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
