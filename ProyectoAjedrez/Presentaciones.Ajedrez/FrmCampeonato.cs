﻿using System;
using System.Windows.Forms;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
    public partial class FrmCampeonato : Form
    {
        LogicaCampeonato _camp = new LogicaCampeonato();
        int fila = 0;
        public FrmCampeonato()
        {
            InitializeComponent();
        }
        void Unable(Boolean id, Boolean guardar, Boolean eliminar, Boolean editar)
        {
            BtnEliminar.Enabled = eliminar;
            BtnGuardar.Enabled = guardar;
            BtnModificar.Enabled = editar;
            txtIdCamp.Enabled = id;
        }
        void Actualizar()
        {
            DtgCampeonato.DataSource = _camp.Mostrar(string.Format(
                "select * from Campeonato where nombre like '%{0}%'",
                TxtBuscar.Text), "Campeonato").Tables[0];
            DtgCampeonato.AutoResizeColumns();
        }
        void Limpiar()
        {
            txtIdCamp.Clear();
            cmbParticipante.Text = "";
            txtNombre.Clear();
            txtTipo.Clear();
        }
        private void FrmCampeonato_Load(object sender, EventArgs e)
        {
            Unable(true, true, false, false);
            _camp.llenarComboP(cmbParticipante, "select nombre from Participante", "Participante");
            cmbParticipante.Text = "";
            Actualizar();
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                int participante = _camp.ObtenerId(cmbParticipante.Text);
                _camp.Insertar(new Campeonato(int.Parse(txtIdCamp.Text), txtNombre.Text, txtTipo.Text, participante));
                Actualizar();
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Caracteres invalidos" + ex);
            }
        }
        private void DtgCampeonato_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            txtIdCamp.Text = DtgCampeonato.Rows[fila].Cells[0].Value.ToString();
            txtNombre.Text = DtgCampeonato.Rows[fila].Cells[1].Value.ToString();
            txtTipo.Text = DtgCampeonato.Rows[fila].Cells[2].Value.ToString();
            cmbParticipante.Text = _camp.ObtenerParticipante(int.Parse(DtgCampeonato.Rows[fila].Cells[3].Value.ToString()));
            Unable(false, false, true, true);
        }
        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                int participante = _camp.ObtenerId(cmbParticipante.Text);
                _camp.Actualizar(new Campeonato(int.Parse(txtIdCamp.Text), txtNombre.Text, txtTipo.Text, participante));
                Actualizar();
                Limpiar();
                Unable(true, true, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de actualizar, necesitas hacer click en el dato que deseas modificar.");
            }
        }
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Esta seguro de eliminar al movimiento " + txtNombre.Text + "?",
                "Atencion!!!", MessageBoxButtons.YesNo);
                if (rs == DialogResult.Yes)
                {
                    _camp.Borrar(new Campeonato(int.Parse(txtIdCamp.Text), "", "", 0));
                    Limpiar();
                    Actualizar();
                    Unable(true, true, false, false);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de eliminar necesitas hacer click en el dato que deseas eliminar.");
            }
        }
        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}