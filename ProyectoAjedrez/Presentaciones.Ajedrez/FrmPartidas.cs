﻿using System.Windows.Forms;
using System;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
    public partial class FrmPartidas : Form
    {
        int fila = 0;
        LogicaPartidas pm = new LogicaPartidas();
        public FrmPartidas()
        {
            InitializeComponent();
        }

        private void FrmPartidas_Load(object sender, System.EventArgs e)
        {
            lblindicacion.Visible = false;
            BtnModificar.Enabled = false;
            BtnEliminar.Enabled = false;
            try
            {
                pm.LlenarArbitro(CmbArbitro, "select idarbitro from arbitro", "arbitro");
                pm.LlenarHotel(CmbHotel, "select idhotel from hotel", "hotel");
                pm.LlenarSala(CmbSala, "select idsala from sala", "sala");
            }
            catch (Exception)
            {

                this.Show();
            }
            Actualizar();
        }
        public void Limpiar()
        {
            TxtId.Clear();
            TxtEntradas.Clear();
            CmbArbitro.SelectedItem = 1;
            CmbHotel.SelectedItem = 1;
            CmbSala.SelectedItem = 1;
        }
        public void Actualizar()
        {
            DtgPartidas.DataSource = pm.Obtener(string.Format("select idpartida as Partida, jornada as Jornada,fkidarbitro as ID_Arbitro, fkidhotel as ID_Hotel, fkidsala as ID_Sala, entradas as Entradas from partida where idpartida like '%{0}%'", TxtBuscar.Text), "partida").Tables[0];
            DtgPartidas.AutoResizeColumns();
            DtgPartidas.AutoResizeRows();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                pm.Guardar(new Partidas(int.Parse(TxtId.Text), DtpJornada.Text, int.Parse(CmbArbitro.Text), int.Parse(CmbHotel.Text), int.Parse(CmbSala.Text), int.Parse(TxtEntradas.Text)));
                Actualizar();
                Limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show("Caractéres invalidos");
            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                TxtId.Enabled = true;
                BtnGuardar.Enabled = true;
                pm.Actualizar(new Partidas(int.Parse(TxtId.Text), DtpJornada.Text, int.Parse(CmbArbitro.Text), int.Parse(CmbHotel.Text), int.Parse(CmbSala.Text), int.Parse(TxtEntradas.Text)));
                Actualizar();
                Limpiar();
                BtnModificar.Enabled = false;
                BtnEliminar.Enabled = false;
                lblindicacion.Visible = false;
            }
            catch (Exception)
            {
                MessageBox.Show("No se realizo ningun cambio, error de dato");
                TxtId.Enabled = true;
                BtnGuardar.Enabled = true;
                Limpiar();
                BtnModificar.Enabled = false;
                lblindicacion.Visible = false;
                BtnEliminar.Enabled = false;
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Esta seguro de eliminar la Partida " + TxtId.Text + " del dia " + DtpJornada.Text + "?",
                "Atencion!!!", MessageBoxButtons.YesNoCancel);
                if (rs == DialogResult.Yes)
                {
                    pm.Eliminar(new Partidas(int.Parse(TxtId.Text), "", 0, 0, 0, 0));
                    Limpiar();
                    Actualizar();
                    BtnEliminar.Enabled = false;
                    BtnGuardar.Enabled = true;
                    BtnModificar.Enabled = false;
                    TxtId.Enabled = true;
                    lblindicacion.Visible = false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de eliminar necesitas hacer click en el dato que deseas eliminar.");
                BtnGuardar.Enabled = true;
                BtnEliminar.Enabled = false;
                BtnModificar.Enabled = false;
                TxtId.Enabled = true;
                lblindicacion.Visible = false;
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void DtgPartidas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TxtId.Enabled = false;
            BtnGuardar.Enabled = false;
            BtnModificar.Enabled = true;
            BtnEliminar.Enabled = true;
            lblindicacion.Visible = true;
            try
            {
                fila = e.RowIndex;
                TxtId.Text = DtgPartidas.Rows[fila].Cells[0].Value.ToString();
                DtpJornada.Text = DtgPartidas.Rows[fila].Cells[1].Value.ToString();
                CmbArbitro.Text = DtgPartidas.Rows[fila].Cells[2].Value.ToString();
                CmbHotel.Text = DtgPartidas.Rows[fila].Cells[3].Value.ToString();
                CmbSala.Text = DtgPartidas.Rows[fila].Cells[4].Value.ToString();
                TxtEntradas.Text = DtgPartidas.Rows[fila].Cells[5].Value.ToString();
            }
            catch (Exception)
            {

                MessageBox.Show("Para actualizar y eliminar da clic sobre el dato que deseas manipular");
            }
        }
    }
}
