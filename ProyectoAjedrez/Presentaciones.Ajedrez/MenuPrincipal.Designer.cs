﻿namespace Presentaciones.Ajedrez
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.paisesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OptPais = new System.Windows.Forms.ToolStripMenuItem();
            this.participantesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OptParticipante = new System.Windows.Forms.ToolStripMenuItem();
            this.OptCampeonato = new System.Windows.Forms.ToolStripMenuItem();
            this.OptJugador = new System.Windows.Forms.ToolStripMenuItem();
            this.OptArbitro = new System.Windows.Forms.ToolStripMenuItem();
            this.hotelesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OptHotel = new System.Windows.Forms.ToolStripMenuItem();
            this.OptReserva = new System.Windows.Forms.ToolStripMenuItem();
            this.OptSala = new System.Windows.Forms.ToolStripMenuItem();
            this.OptPartida = new System.Windows.Forms.ToolStripMenuItem();
            this.partidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OptJuego = new System.Windows.Forms.ToolStripMenuItem();
            this.OptMovimientos = new System.Windows.Forms.ToolStripMenuItem();
            this.OptSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paisesToolStripMenuItem,
            this.participantesToolStripMenuItem,
            this.hotelesToolStripMenuItem,
            this.OptPartida,
            this.OptSalir});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // paisesToolStripMenuItem
            // 
            this.paisesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptPais});
            this.paisesToolStripMenuItem.Image = global::Presentaciones.Ajedrez.Properties.Resources.Pais;
            this.paisesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.paisesToolStripMenuItem.Name = "paisesToolStripMenuItem";
            this.paisesToolStripMenuItem.Size = new System.Drawing.Size(90, 36);
            this.paisesToolStripMenuItem.Text = "Paises";
            // 
            // OptPais
            // 
            this.OptPais.Name = "OptPais";
            this.OptPais.Size = new System.Drawing.Size(99, 22);
            this.OptPais.Text = "Pais";
            this.OptPais.Click += new System.EventHandler(this.OptPais_Click);
            // 
            // participantesToolStripMenuItem
            // 
            this.participantesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptParticipante,
            this.OptCampeonato,
            this.OptJugador,
            this.OptArbitro});
            this.participantesToolStripMenuItem.Image = global::Presentaciones.Ajedrez.Properties.Resources.Participantes;
            this.participantesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.participantesToolStripMenuItem.Name = "participantesToolStripMenuItem";
            this.participantesToolStripMenuItem.Size = new System.Drawing.Size(127, 36);
            this.participantesToolStripMenuItem.Text = "Participantes";
            // 
            // OptParticipante
            // 
            this.OptParticipante.Name = "OptParticipante";
            this.OptParticipante.Size = new System.Drawing.Size(146, 22);
            this.OptParticipante.Text = "Participante";
            this.OptParticipante.Click += new System.EventHandler(this.OptParticipante_Click);
            // 
            // OptCampeonato
            // 
            this.OptCampeonato.Name = "OptCampeonato";
            this.OptCampeonato.Size = new System.Drawing.Size(146, 22);
            this.OptCampeonato.Text = "Campeonato";
            this.OptCampeonato.Click += new System.EventHandler(this.OptCampeonato_Click);
            // 
            // OptJugador
            // 
            this.OptJugador.Name = "OptJugador";
            this.OptJugador.Size = new System.Drawing.Size(146, 22);
            this.OptJugador.Text = "Jugador";
            this.OptJugador.Click += new System.EventHandler(this.OptJugador_Click);
            // 
            // OptArbitro
            // 
            this.OptArbitro.Name = "OptArbitro";
            this.OptArbitro.Size = new System.Drawing.Size(146, 22);
            this.OptArbitro.Text = "Arbitro";
            this.OptArbitro.Click += new System.EventHandler(this.OptArbitro_Click);
            // 
            // hotelesToolStripMenuItem
            // 
            this.hotelesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptHotel,
            this.OptReserva,
            this.OptSala});
            this.hotelesToolStripMenuItem.Image = global::Presentaciones.Ajedrez.Properties.Resources.Hotel__1_;
            this.hotelesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.hotelesToolStripMenuItem.Name = "hotelesToolStripMenuItem";
            this.hotelesToolStripMenuItem.Size = new System.Drawing.Size(94, 36);
            this.hotelesToolStripMenuItem.Text = "Hoteles";
            // 
            // OptHotel
            // 
            this.OptHotel.Name = "OptHotel";
            this.OptHotel.Size = new System.Drawing.Size(121, 22);
            this.OptHotel.Text = "Hotel";
            this.OptHotel.Click += new System.EventHandler(this.OptHotel_Click);
            // 
            // OptReserva
            // 
            this.OptReserva.Name = "OptReserva";
            this.OptReserva.Size = new System.Drawing.Size(121, 22);
            this.OptReserva.Text = "Reserva";
            this.OptReserva.Click += new System.EventHandler(this.OptReserva_Click);
            // 
            // OptSala
            // 
            this.OptSala.Name = "OptSala";
            this.OptSala.Size = new System.Drawing.Size(121, 22);
            this.OptSala.Text = "Sala";
            this.OptSala.Click += new System.EventHandler(this.OptSala_Click);
            // 
            // OptPartida
            // 
            this.OptPartida.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.partidaToolStripMenuItem,
            this.OptJuego,
            this.OptMovimientos});
            this.OptPartida.Image = global::Presentaciones.Ajedrez.Properties.Resources.Partidas;
            this.OptPartida.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptPartida.Name = "OptPartida";
            this.OptPartida.Size = new System.Drawing.Size(99, 36);
            this.OptPartida.Text = "Partidas";
            // 
            // partidaToolStripMenuItem
            // 
            this.partidaToolStripMenuItem.Name = "partidaToolStripMenuItem";
            this.partidaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.partidaToolStripMenuItem.Text = "Partida";
            this.partidaToolStripMenuItem.Click += new System.EventHandler(this.partidaToolStripMenuItem_Click);
            // 
            // OptJuego
            // 
            this.OptJuego.Name = "OptJuego";
            this.OptJuego.Size = new System.Drawing.Size(180, 22);
            this.OptJuego.Text = "Juego";
            this.OptJuego.Click += new System.EventHandler(this.OptJuego_Click);
            // 
            // OptMovimientos
            // 
            this.OptMovimientos.Name = "OptMovimientos";
            this.OptMovimientos.Size = new System.Drawing.Size(180, 22);
            this.OptMovimientos.Text = "Movimientos";
            this.OptMovimientos.Click += new System.EventHandler(this.OptMovimientos_Click);
            // 
            // OptSalir
            // 
            this.OptSalir.Image = global::Presentaciones.Ajedrez.Properties.Resources.Salir;
            this.OptSalir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptSalir.Name = "OptSalir";
            this.OptSalir.Size = new System.Drawing.Size(77, 36);
            this.OptSalir.Text = "Salir";
            this.OptSalir.Click += new System.EventHandler(this.OptSalir_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Presentaciones.Ajedrez.Properties.Resources.Fondo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(760, 506);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MenuPrincipal";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Club de Ajedrez Lagos de Moreno";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem paisesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OptPais;
        private System.Windows.Forms.ToolStripMenuItem participantesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OptParticipante;
        private System.Windows.Forms.ToolStripMenuItem OptCampeonato;
        private System.Windows.Forms.ToolStripMenuItem hotelesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OptPartida;
        private System.Windows.Forms.ToolStripMenuItem OptSalir;
        private System.Windows.Forms.ToolStripMenuItem OptJugador;
        private System.Windows.Forms.ToolStripMenuItem OptArbitro;
        private System.Windows.Forms.ToolStripMenuItem OptHotel;
        private System.Windows.Forms.ToolStripMenuItem OptReserva;
        private System.Windows.Forms.ToolStripMenuItem OptSala;
        private System.Windows.Forms.ToolStripMenuItem partidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OptJuego;
        private System.Windows.Forms.ToolStripMenuItem OptMovimientos;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}