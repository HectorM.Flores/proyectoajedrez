﻿using System;
using System.Windows.Forms;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
    public partial class FrmLogin : Form
    {
        LogicaLogin ln = new LogicaLogin(); 
        public FrmLogin()
        {
            InitializeComponent();
        }

        public string Validar(string usuario, string contraseña)
        {
            if (!ln.ValidarUsuario(usuario).Equals("false") && !ln.ValidarContraseña(contraseña).Equals("false"))
                return "Bienvenido";
            else
                return "Usuario o contraseña incorrecta";
        }

        private void BtnIniciarSesion_Click(object sender, EventArgs e)
        {
            if (Validar(TxtUsuario.Text, TxtContraseña.Text).Equals("Bienvenido"))
            {
                try
                {
                    this.Hide();
                    FrmLoad fl = new FrmLoad();
                    fl.ShowDialog();
                }
                catch (Exception)
                {
                }
                
            }
            else
            {
                MessageBox.Show(Validar(TxtUsuario.Text, TxtContraseña.Text));
                TxtUsuario.Clear();
                TxtContraseña.Clear();
            }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            TxtContraseña.PasswordChar = '*';
        }
    }
}
