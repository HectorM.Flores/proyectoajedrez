﻿using Entidades.Ajedrez;
using AccesoDatos.Ajedrez;
using System.Data;
using System.Windows.Forms;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaSala
    {
        AccesoDatosSala s = new AccesoDatosSala();

        public void Insertar(Sala sala)
        {
            s.Insertar(sala);
        }
        public void Actualizar(Sala sala)
        {
            s.Actualizar(sala);
        }
        public void Eliminar(Sala sala)
        {
            s.Eliminar(sala);
        }
        public DataSet Mostrar(string script, string tabla)
        {
            return s.Listado(script, tabla);
        }
        public void llenarCombo(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = s.Listado(script, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
        public int Obteneridhotel(string nombre)
        {
            return s.TraerIdHotel(nombre);
        }
        public string ObtenerNombrehotel(int idhotel)
        {
            return s.GetHotel(idhotel);
        }
    }
}
