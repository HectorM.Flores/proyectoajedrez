﻿using Entidades.Ajedrez;
using AccesoDatos.Ajedrez;
using System.Data;
using System.Windows.Forms;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaReserva
    {
        AccesoDatosReserva r = new AccesoDatosReserva();
        public void Insertar(Reserva reserva)
        {
            r.Insertar(reserva);
        }
        public void Actualizar(Reserva reserva)
        {
            r.Actualizar(reserva);
        }
        public void Eliminar(Reserva reserva)
        {
            r.Eliminar(reserva);
        }
        public DataSet Mostrar(string script, string tabla)
        {
            return r.Listado(script, tabla);
        }
        public void llenarCombo(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = r.Listado(script, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
        public int ObtenerIdparticipante(string nombre)
        {
            return r.TraerIdParticipante(nombre);
        }
        public string ObtenerNombreparticipante(int nsocio)
        {
            return r.GetParticipante(nsocio);
        }
        public int Obteneridhotel(string nombre)
        {
            return r.TraerIdHotel(nombre);
        }
        public string ObtenerNombrehotel(int idhotel)
        {
            return r.GetHotel(idhotel);
        }
        public void llenarCombo2(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = r.Listado(script, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
    }
}
