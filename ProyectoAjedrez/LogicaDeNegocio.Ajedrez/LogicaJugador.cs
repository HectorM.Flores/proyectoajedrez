﻿using System.Data;
using System.Windows.Forms;
using AccesoDatos.Ajedrez;
using Entidades.Ajedrez;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaJugador
    {
        AccesoDatosJugador jad = new AccesoDatosJugador();

        public int GetIdParticipante(string participante)
        {
            return jad.GetIdParticipante(participante);
        }

        public string GetParticipante(int nsocio)
        {
            return jad.GetParticipante(nsocio);
        }

        public void Insertar(Jugador jugadores)
        {
            jad.Insertar(jugadores);
        }

        public void Actualizar(Jugador jugadores)
        {
            jad.Actualizar(jugadores);
        }

        public void Borrar(Jugador jugadores)
        {
            jad.Borrar(jugadores);
        }

        public DataSet Listado(string q, string tabla)
        {
            return jad.Listado(q, tabla);
        }

        public void LlenarParticipantes(ComboBox combo, string q, string tabla)
        {
            jad.LlenarParticipantes(combo, q, tabla);
        }
    }
}
