﻿using AccesoDatos.Ajedrez;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaLogin
    {
        AccesoDatosLogin acd = new AccesoDatosLogin();
        public string ValidarUsuario(string usuario)
        {
            return acd.ValidarUsuario(usuario);
        }

        public string ValidarContraseña(string contraseña)
        {
            return acd.ValidarContraseña(contraseña);
        }
    }
}
