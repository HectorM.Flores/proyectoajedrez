﻿using Entidades.Ajedrez;
using AccesoDatos.Ajedrez;
using System.Data;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaHotel
    {
        AccesoDatosHotel ah = new AccesoDatosHotel();
        public void Insertar(Hotel hotel)
        {
            ah.Insertar(hotel);
        }

        public void Eliminar(Hotel hotel)
        {
            ah.Eliminar(hotel);
        }

        public void Actualizar(Hotel hotel)
        {
            ah.Actualizar(hotel);
        }

        public DataSet Mostrar(string script, string tabla)
        {
            return ah.Listado(script, tabla);
        }
    }
}
