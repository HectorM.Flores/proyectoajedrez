﻿using System.Data;
using System.Windows.Forms;
using AccesoDatos.Ajedrez;
using Entidades.Ajedrez;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaArbitro
    {
        AccesoDatosArbitro aad = new AccesoDatosArbitro();

        public string GetParticipante(int nsocio)
        {
            return aad.GetParticipante(nsocio);
        }

        public void Insertar(Arbitro arbitros)
        {
            aad.Insertar(arbitros);
        }

        public void Borrar(Arbitro arbitros)
        {
            aad.Borrar(arbitros);
        }

        public DataSet Listado(string q, string tabla)
        {
            return aad.Listado(q, tabla);
        }

        public void LlenarParticipantes(ComboBox combo, string q, string tabla)
        {
            aad.LlenarParticipantes(combo, q, tabla);
        }
    }
}
