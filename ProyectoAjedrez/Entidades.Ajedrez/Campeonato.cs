﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Ajedrez
{
    public class Campeonato
    {
        public int IdCampeonato { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int IdParticipante { get; set; }
        public Campeonato(int idcampeonato, string nombre, string tipo, int idparticipante)
        {
            IdCampeonato = idcampeonato;
            Nombre = nombre;
            Tipo = tipo;
            IdParticipante = idparticipante;
        }
    }
}
