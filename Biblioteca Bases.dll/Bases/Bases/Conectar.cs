﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace Bases
{
    public class Conectar
    {
        MySqlConnection con;
        public Conectar(string server, string user, string pwd, string bd)
        {
            con = new MySqlConnection("server="+server+"; user="+user+
                "; password="+pwd+"; database="+bd);
        }
        //Metodo para insertar actualizar, borrar, etc... Transacciones
        public string Comando(string q)
        {
            try
            {   //insert into datos values(null, 'Nestor', 24);
                MySqlCommand i = new MySqlCommand(q, con);
                con.Open();
                i.ExecuteNonQuery(); //confirmar a mysql la transaccion.
                con.Close();
                return "Correcto";
            }
            catch (Exception ex)
            {
                con.Close();
                return "Error, " + ex.Message;
            }
        }
        //Metodo para obtener datos
        public DataSet Consultar(string q, string tabla)
        {
            DataSet ds = new DataSet(); //Contenedor para obtener los registros
            try
            {
                MySqlDataAdapter da = new MySqlDataAdapter(q, con);
                con.Open();
                da.Fill(ds, tabla); //Trasladar los datos desde el adaptador al dataset
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
            return ds;
        }
    }
}
